#include "stanley_control.h"

#include <algorithm>
#include <iomanip>
#include <utility>
#include <vector>

#include "Eigen/LU"
#include <math.h>

using namespace std;

namespace shenlan {
namespace control {

double atan2_to_PI(const double atan2) {
  return atan2 * M_PI / 180;
}

double PointDistanceSquare(const TrajectoryPoint &point, const double x,
                           const double y) {
  const double dx = point.x - x;
  const double dy = point.y - y;
  return dx * dx + dy * dy;
}

void StanleyController::LoadControlConf() {
  k_y_ = 0.5;
}

// /** to-do **/ 计算需要的控制命令, 实现对应的stanley模型,并将获得的控制命令传递给汽车
// 提示，在该函数中你需要调用计算误差
void StanleyController::ComputeControlCmd(
    const VehicleState &vehicle_state,
    const TrajectoryData &planning_published_trajectory, ControlCmd &cmd) {
  //定义一些初始变量
  double vehicle_x = vehicle_state.x;
  double vehicle_y = vehicle_state.y;
  double vehicle_theta = vehicle_state.heading;
  double vehicle_vel = vehicle_state.velocity;
  double e_y;
  double e_theta;

  
  //复制轨迹点
  trajectory_points_.clear();
  for(std::size_t i = 0; i < planning_published_trajectory.trajectory_points.size(); i++)
  {
    trajectory_points_.push_back(planning_published_trajectory.trajectory_points[i]);
  }

  //计算需要的误差
  ComputeLateralErrors(vehicle_x,vehicle_y,vehicle_theta,e_y,e_theta);

  //更新增益
  StanleyController::LoadControlConf();
  
  double e_delta = atan2(k_y_ * e_y , vehicle_vel);

  cmd.steer_target = e_theta + e_delta;
  //cmd.acc = 1;
}

// /** to-do **/ 计算需要的误差，包括横向误差，纵向误差

void StanleyController::ComputeLateralErrors(const double x, const double y,
                                             const double theta, double &e_y,
                                             double &e_theta) {
  double dx = 0.0;
  double dy = 0.0;
  double tx = 0.0;
  double ty = 0.0;
  double theta_fi = 0.0;
  TrajectoryPoint near_point;
  //寻找当前点离轨迹点的最近点                                             
  near_point = QueryNearestPointByPosition(x,y);
  //计算横向误差
  dx = x - near_point.x;
  dy = y - near_point.y;
  tx = cos(near_point.heading);
  ty = sin(near_point.heading);
  e_y = sqrt(dx*dx + dy * dy);
  double fx = dy * tx - dx * ty;
  if(fx > 0 )
  {
    e_y = -abs(e_y);
  }else{
    e_y = abs(e_y);
  }

  //计算航向误差
  theta_fi = -(near_point.heading - theta);

  if (theta_fi > (-M_PI / 2) && (theta_fi < (M_PI / 2))) 
  {
    e_theta = theta_fi;
  }  
}

TrajectoryPoint StanleyController::QueryNearestPointByPosition(const double x,
                                                               const double y) {
  double d_min = PointDistanceSquare(trajectory_points_.front(), x, y);
  size_t index_min = 0;

  for (size_t i = 1; i < trajectory_points_.size(); ++i) {
    double d_temp = PointDistanceSquare(trajectory_points_[i], x, y);
    if (d_temp < d_min) {
      d_min = d_temp;
      index_min = i;
    }
  }
  // cout << " index_min: " << index_min << endl;
  //cout << "tarjectory.heading: " << trajectory_points_[index_min].heading << endl;
  theta_ref_ = trajectory_points_[index_min].heading;

  return trajectory_points_[index_min];
}

}  // namespace control
}  // namespace shenlan

