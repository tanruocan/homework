
"use strict";

let TestArrayRequest = require('./TestArrayRequest.js')
let SendBytes = require('./SendBytes.js')
let TestResponseOnly = require('./TestResponseOnly.js')
let TestMultipleRequestFields = require('./TestMultipleRequestFields.js')
let AddTwoInts = require('./AddTwoInts.js')
let TestRequestAndResponse = require('./TestRequestAndResponse.js')
let TestNestedService = require('./TestNestedService.js')
let TestRequestOnly = require('./TestRequestOnly.js')
let TestEmpty = require('./TestEmpty.js')
let TestMultipleResponseFields = require('./TestMultipleResponseFields.js')

module.exports = {
  TestArrayRequest: TestArrayRequest,
  SendBytes: SendBytes,
  TestResponseOnly: TestResponseOnly,
  TestMultipleRequestFields: TestMultipleRequestFields,
  AddTwoInts: AddTwoInts,
  TestRequestAndResponse: TestRequestAndResponse,
  TestNestedService: TestNestedService,
  TestRequestOnly: TestRequestOnly,
  TestEmpty: TestEmpty,
  TestMultipleResponseFields: TestMultipleResponseFields,
};
