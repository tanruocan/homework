/**
 * @Author: YunKai Xia
 * @Date:   2022-07-10 22:28:51
 * @Last Modified by:   YunKai Xia
 * @Last Modified time: 2022-07-18 22:26:04
 */

// ***Description***:
// Many thanks to the author of the Frenet algorithm here, this paper may be
// very helpful to you, "Optimal Trajectory Generation for Dynamic Street
// Scenarios in a Frenet Frame"
// https://www.researchgate.net/publication/224156269_Optimal_Trajectory_Generation_for_Dynamic_Street_Scenarios_in_a_Frenet_Frame
// Thanks to open source codes, python robotics, this website can help you
// quickly verify some algorithms, which is very useful for beginners.
// https://github.com/AtsushiSakai/PythonRobotics

#include "frenet_optimal_trajectory.h"

#include "ros/ros.h"

namespace shenlan {
#define MAX_SPEED 50.0 / 3.6     // maximum speed [m/s]
#define MAX_ACCEL 2.0            // maximum acceleration [m/ss]
#define MAX_CURVATURE 1.0        // maximum curvature [1/m]
#define MAX_ROAD_WIDTH 7.0       // maximum road width [m]
#define D_ROAD_W 1.0             // road width sampling length [m]
#define DT 0.2                   // time tick [s]
#define MAXT 5.0                 // max prediction time [m]
#define MINT 4.0                 // min prediction time [m]
#define TARGET_SPEED 30.0 / 3.6  // target speed [m/s]
#define D_T_S 5.0 / 3.6          // target speed sampling length [m/s]
#define N_S_SAMPLE 1             // sampling number of target speed
#define ROBOT_RADIUS 1.5         // robot radius [m]

#define KJ 0.1
#define KT 0.1
#define KD 1.0
#define KLAT 1.0
#define KLON 1.0

#define FLAGS_weight_target_speed 1
#define FLAGS_weight_dist_travelled 10
#define FLAGS_longitudinal_jerk_upper_bound 2
#define FLAGS_numerical_epsilon 1
#define FLAGS_lat_offset_bound 3
#define FLAGS_weight_opposite_side_offset 10
#define FLAGS_weight_same_side_offset 1
#define FLAGS_weight_lon_objective 10.0
#define FLAGS_weight_lon_jerk 1.0
#define FLAGS_weight_centripetal_acceleration 1.5
#define FLAGS_weight_lat_offset 2.0
#define FLAGS_weight_lat_comfort 10.0

FrenetOptimalTrajectory::FrenetOptimalTrajectory() {}
FrenetOptimalTrajectory::~FrenetOptimalTrajectory() {}

double LonObjectiveCost(const FrenetPath &fp)
{
  double dist_s =
      fp.s.back() - fp.s.front();

  double speed_cost_sqr_sum = 0.0;
  double speed_cost_weight_sum = 0.0;
  for (size_t i = 0; i < fp.s.size(); ++i) {
    double cost = fp.s_d[i] - TARGET_SPEED;
    speed_cost_sqr_sum += fp.t[i] * fp.t[i] * std::fabs(cost);
    speed_cost_weight_sum += fp.t[i] * fp.t[i];
  }
  //关于速度的代价 速度越接近目标值，cost越小，t^2放大这一误差
  double speed_cost =
      speed_cost_sqr_sum / (speed_cost_weight_sum + 1e-6);
  //关于距离的代价
  double dist_travelled_cost = 1.0 / (1.0 + dist_s);
  return (speed_cost * FLAGS_weight_target_speed +
          dist_travelled_cost * FLAGS_weight_dist_travelled) /
         (FLAGS_weight_target_speed + FLAGS_weight_dist_travelled);
}

double LonComfortCost(const FrenetPath& fp)
{
  double cost_sqr_sum = 0.0;
  double cost_abs_sum = 0.0;
  for (size_t i = 0; i < fp.s.size();++i)
  {
    double jerk = fp.s_ddd[i];
    //舒适度关于到jerk
    double cost = jerk / FLAGS_longitudinal_jerk_upper_bound;
    cost_sqr_sum += cost * cost;
    cost_abs_sum += std::fabs(cost);
  }
  return cost_sqr_sum / (cost_abs_sum + 1e-6);
}

//向心加速度
double CentripetalAccelerationCost(const FrenetPath& fp)
{
  // Assumes the vehicle is not obviously deviate from the reference line.
  double centripetal_acc_sum = 0.0;
  double centripetal_acc_sqr_sum = 0.0;
  for(size_t i = 0; i < fp.s.size();++i)
  {
    double s = fp.s[i];
    double v = fp.s_d[i];
    double centripetal_acc = v * v * fp.c[i];
    centripetal_acc_sum += std::fabs(centripetal_acc);
    centripetal_acc_sqr_sum += centripetal_acc * centripetal_acc;
  }

  return centripetal_acc_sqr_sum /
         (centripetal_acc_sum + FLAGS_numerical_epsilon);
}

//横向偏移代价
double LatOffsetCost(const FrenetPath& fp)
{
  double lat_offset_start = fp.d[0];
  double cost_sqr_sum = 0.0;
  double cost_abs_sum = 0.0;
  for (size_t i = 0; i < fp.s.size(); ++i)
  {
    double lat_offset = fp.d[i];
    double cost = lat_offset / FLAGS_lat_offset_bound;
    if (lat_offset * lat_offset_start < 0.0) //方向相反
    {
      cost_sqr_sum += cost * cost * FLAGS_weight_opposite_side_offset;
      cost_abs_sum += std::fabs(cost) * FLAGS_weight_opposite_side_offset;
    } else {
      cost_sqr_sum += cost * cost * FLAGS_weight_same_side_offset;
      cost_abs_sum += std::fabs(cost) * FLAGS_weight_same_side_offset;
    }
  }
  return cost_sqr_sum / (cost_abs_sum + 1e-6);
}

//横向舒适cost
double LatComfortCost(const FrenetPath& fp)
{
  double max_cost = 0.0;
  for (size_t i = 0; i < fp.s.size(); ++i)
  {
    double s = fp.s[i];
    double s_dot = fp.s_d[i];
    double s_dotdot = fp.s_dd[i];

    double d = fp.d[i];
    double d_dot = fp.d_d[i];
    double d_dotdot = fp.d_dd[i];
    double cost = d_dotdot * s_dot * s_dot + d_dot * s_dotdot;
    max_cost = std::max(max_cost, std::fabs(cost));
  }
  return max_cost;
}

float FrenetOptimalTrajectory::sum_of_power(std::vector<float> value_list) {
  float sum = 0;
  for (float item : value_list) {
    sum += item * item;
  }
  return sum;
};

// 01 获取采样轨迹
Vec_Path FrenetOptimalTrajectory::calc_frenet_paths(float c_speed, float c_d,
                                                    float c_d_d, float c_d_dd,
                                                    float s0) {
  std::vector<FrenetPath> fp_list;
  //完成轨迹采样
  
  //横向采样
  for(float di = -1 * MAX_ROAD_WIDTH; di < MAX_ROAD_WIDTH; di+=D_ROAD_W)
  {
    //规划实践在 MINT - MAXT
    for(float ti = MINT; ti < MAXT ; ti += DT)
    {
        FrenetPath fp;
        //-----------横向规划 d(t)------------//
        //start:p、v、a       end:p、0、0（最终横向速度和加速度都为0）  t：ti
        QuinticPolynomial lat_qp(c_d ,c_d_d ,c_d_dd ,di ,0 ,0 ,ti );//五次多项式
        for(float t = 0;t < ti; t += DT)
        {
            fp.t.push_back(t);
            fp.d.push_back(lat_qp.calc_point(t));
            fp.d_d.push_back(lat_qp.calc_first_derivative(t));//v
            fp.d_dd.push_back(lat_qp.calc_second_derivative(t));//a
            fp.d_ddd.push_back(lat_qp.calc_third_derivative(t));//jerk
        }

        //-----------纵向规划 d(t)------------//
        float min_variance_speed = TARGET_SPEED - D_T_S * N_S_SAMPLE;
        float max_variance_speed = TARGET_SPEED + D_T_S * N_S_SAMPLE;

        //采样速度
        for(float tv = min_variance_speed; tv < max_variance_speed ; tv+=D_T_S)
        {
            FrenetPath tfp = fp;
            //start：p、v、a=0     end：v、a= 0（因为巡航，所以没有确定的中点位置）
            QuarticPolynomial lon_qp(s0,c_speed,0.0,tv,0.0,ti);//四次多项式
            
            tfp.max_speed = std::numeric_limits<float>::min();
            tfp.max_accel = std::numeric_limits<float>::min();
            
            for(float t_:fp.t)
            {
                tfp.s.push_back(lon_qp.calc_point(t_));
                tfp.s_d.push_back(lon_qp.calc_first_derivative(t_));
                tfp.s_dd.push_back(lon_qp.calc_second_derivative(t_));
                tfp.s_ddd.push_back(lon_qp.calc_third_derivative(t_));
                
                //更新最大速度和加速度
                if(tfp.s_d.back()>tfp.max_speed)
                {
                    tfp.max_speed = tfp.s_d.back();
                }
                 if(tfp.s_dd.back()>tfp.max_accel)
                {
                    tfp.max_accel = tfp.s_dd.back();
                }
            }

            //计算代价
            float Jp = sum_of_power(tfp.d_ddd);
            float Js = sum_of_power(tfp.s_ddd);

            //纵向速度和目标速度的差
            double ds = (TARGET_SPEED - tfp.s_d.back());

            //横向误差损失函数
            tfp.cd = KJ * Jp + KT * ti + KD * std::pow(tfp.d.back(),2);
            //纵向损失函数
            tfp.cv = KJ * Js + KT * ti + KD * ds * ds;
            //总的损失函数
            tfp.cf = KLAT * tfp.cd + KLON * tfp.cv;

            fp_list.push_back(tfp);
        }
    }
  }
  return fp_list;
};

// 02
// 根据参考轨迹与采样的轨迹数组，计算frenet中的其他曲线参数，如航向角，曲率，ds等参数
void FrenetOptimalTrajectory::calc_global_paths(Vec_Path& path_list,
                                                Spline2D csp){
    //计算采样轨迹的其他参数
    //遍历每个轨迹
    for(Vec_Path::iterator path_p = path_list.begin(); path_p!=path_list.end(); path_p++)
    {   
        //遍历每个轨迹上的每个点 
        for(size_t i = 0; i < path_p->s.size(); ++i)
        {
            if(path_p->s[i] >= csp.s.back())
            {
                break;
            }
            //计算笛卡尔坐标系下的X、Y
            std::array<float,2> pose = csp.calc_postion(path_p->s[i]);
            float yaw_i = csp.calc_yaw(path_p->s[i]);
            std::cout<<"yaw_i :" <<yaw_i <<endl;
            float d_i = path_p->d[i];
            float x = pose[i] + d_i * std::cos(yaw_i + M_PI/2.0);
            float y = pose[i] + d_i * std::sin(yaw_i + M_PI/2.0);
            path_p->x.push_back(x);
            path_p->y.push_back(y);
        }

        //计算yaw和ds
        for(size_t i = 0; i < path_p->x.size() - 1; ++i)
        {
            float dx = path_p->x[i+1] - path_p->x[i];
            float dy = path_p->y[i+1] - path_p->y[i];
            
            float yaw = std::atan2(dy,dx);
            std::cout<<"yaw :" <<yaw <<endl;
            float ds = std::sqrt(dx * dx + dy * dy);
            path_p->yaw.push_back(yaw);
            path_p->ds.push_back(ds);
        }
        path_p->yaw.push_back(path_p->yaw.back());
        path_p->ds.push_back(path_p->ds.back());

        //计算曲率
        path_p->max_curvature = std::numeric_limits<float>::min();
        for(size_t i = 0; i < path_p->x.size() -1 ; i++)
        {
            float cur = ((path_p->yaw[i+1] - path_p->yaw[i])/ path_p->ds[i]);
            path_p->c.push_back(cur);
            if(cur > path_p->max_curvature)
            {
                path_p->max_curvature = cur;
            }
        }

        double lon_obj_cost = LonObjectiveCost(*path_p);
        double lon_comfort_cost = LonComfortCost(*path_p);
        double centripetal_acc_cost = CentripetalAccelerationCost(*path_p);
        double lat_offset_cost = LatOffsetCost(*path_p);
        double lat_comfort_cost = LatComfortCost(*path_p);

        #ifdef apollo_cost
        path_p->cf = lon_obj_cost * FLAGS_weight_lon_objective + //10
         lon_comfort_cost * FLAGS_weight_lon_jerk + //1
         centripetal_acc_cost * FLAGS_weight_centripetal_acceleration + //1.5
         lat_offset_cost * FLAGS_weight_lat_offset + //2.0
         lat_comfort_cost * FLAGS_weight_lat_comfort; // 10
        #endif
    }
};

bool FrenetOptimalTrajectory::check_collision(FrenetPath path,
                                              const Vec_Poi ob) {
  for (auto point : ob) {
    for (unsigned int i = 0; i < path.x.size(); i++) {
      float dist = std::pow((path.x[i] - point[0]), 2) +
                   std::pow((path.y[i] - point[1]), 2);
      if (dist <= ROBOT_RADIUS * ROBOT_RADIUS) {
        return false;
      }
    }
  }
  return true;
};
// 03
// 检查路径，通过限制做大速度，最大加速度，最大曲率与避障，选取可使用的轨迹数组
Vec_Path FrenetOptimalTrajectory::check_paths(Vec_Path path_list,
                                              const Vec_Poi ob) {
  Vec_Path output_fp_list;
  //补全代码
  for(FrenetPath path : path_list)
  {
    if(path.max_speed < MAX_SPEED && 
       path.max_accel < MAX_ACCEL && 
       path.max_curvature < MAX_CURVATURE &&
       check_collision(path,ob))
       {
        output_fp_list.push_back(path);
       }
  }
  return output_fp_list;
};
// to-do step 1 finish frenet_optimal_planning
FrenetPath FrenetOptimalTrajectory::frenet_optimal_planning(
    Spline2D csp, float s0, float c_speed, float c_d, float c_d_d, float c_d_dd,
    Vec_Poi ob) {
  // 01 获取采样轨迹数组
  Vec_Path fp_list = calc_frenet_paths(c_speed, c_d, c_d_d, c_d_dd, s0);
  // 02
  // 根据参考轨迹与采样的轨迹数组，计算frenet中的其他曲线参数，如航向角，曲率，ds等参数
  calc_global_paths(fp_list, csp);

  // 03
  // 检查路径，通过限制做大速度，最大加速度，最大曲率与避障，选取可使用的轨迹数组
  Vec_Path save_paths = check_paths(fp_list, ob);

  float min_cost = std::numeric_limits<float>::max();
  FrenetPath final_path;
  for (auto path : save_paths) {
    if (min_cost >= path.cf) {
      min_cost = path.cf;
      final_path = path;
    }
  }

  return final_path;
};

FrenetPath FrenetOptimalTrajectory::frenet_optimal_planning(
    Spline2D csp, const FrenetInitialConditions& frenet_init_conditions,
    Vec_Poi ob) {
  float c_speed = frenet_init_conditions.c_speed;
  float c_d = frenet_init_conditions.c_d;
  float c_d_d = frenet_init_conditions.c_d_d;
  float c_d_dd = frenet_init_conditions.c_d_dd;
  float s0 = frenet_init_conditions.s0;

  Vec_Path fp_list = calc_frenet_paths(c_speed, c_d, c_d_d, c_d_dd, s0);
  calc_global_paths(fp_list, csp);
  Vec_Path save_paths = check_paths(fp_list, ob);

  float min_cost = std::numeric_limits<float>::max();
  FrenetPath final_path;
  for (auto path : save_paths) {
    if (min_cost >= path.cf) {
      min_cost = path.cf;
      final_path = path;
    }
  }
  return final_path;
}

}  // namespace shenlan
