#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/trc/Shenlan-autodriving/homework/project04_ws/devel:$CMAKE_PREFIX_PATH"
export PATH='/opt/ros/melodic/bin:/home/trc/.local/bin/:/home/trc/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/trc/.fishros/bin/'
export PWD='/home/trc/Shenlan-autodriving/homework/project04_ws/build'
export ROSLISP_PACKAGE_DIRECTORIES='/home/trc/Shenlan-autodriving/homework/project04_ws/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/trc/Shenlan-autodriving/homework/project04_ws/src:$ROS_PACKAGE_PATH"