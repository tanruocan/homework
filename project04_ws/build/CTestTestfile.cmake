# CMake generated Testfile for 
# Source directory: /home/trc/Shenlan-autodriving/homework/project04_ws/src
# Build directory: /home/trc/Shenlan-autodriving/homework/project04_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("ros_viz_tools")
subdirs("localization")
subdirs("lqr_control")
subdirs("referenceline")
subdirs("vehicle_control")
subdirs("path_planning")
