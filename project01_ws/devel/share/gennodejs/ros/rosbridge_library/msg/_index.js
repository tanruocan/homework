
"use strict";

let TestHeaderArray = require('./TestHeaderArray.js');
let TestTimeArray = require('./TestTimeArray.js');
let TestChar = require('./TestChar.js');
let TestUInt8 = require('./TestUInt8.js');
let TestDurationArray = require('./TestDurationArray.js');
let TestUInt8FixedSizeArray16 = require('./TestUInt8FixedSizeArray16.js');
let TestHeaderTwo = require('./TestHeaderTwo.js');
let Num = require('./Num.js');
let TestHeader = require('./TestHeader.js');

module.exports = {
  TestHeaderArray: TestHeaderArray,
  TestTimeArray: TestTimeArray,
  TestChar: TestChar,
  TestUInt8: TestUInt8,
  TestDurationArray: TestDurationArray,
  TestUInt8FixedSizeArray16: TestUInt8FixedSizeArray16,
  TestHeaderTwo: TestHeaderTwo,
  Num: Num,
  TestHeader: TestHeader,
};
