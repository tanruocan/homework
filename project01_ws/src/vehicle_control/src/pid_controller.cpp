#include "pid_controller.h"

namespace shenlan {
namespace control {

PIDController::PIDController(const double kp, const double ki,
                             const double kd) {
  kp_ = kp;
  ki_ = ki;
  kd_ = kd;
  previous_error_ = 0.0;
  previous_output_ = 0.0;
  integral_ = 0.0;
  first_hit_ = true;
}

// /**to-do**/ 实现PID控制
double PIDController::Control(const double error, const double dt) {
  integral_ += error * dt;
  previous_output_ = kp_ * error + ki_ * integral_ + kd_ * (error - previous_error_)/dt;
  previous_error_ = error;
  return previous_output_;
  

}

// /**to-do**/ 重置PID参数
void PIDController::Reset() {
  kp_ = 0.0;
  ki_ = 0.0;
  kd_ = 0.0;
  previous_error_ = 0.0;
  previous_output_ = 0.0;
  integral_ = 0.0;
  first_hit_ = true;
}

}  // namespace control
}  // namespace shenlan