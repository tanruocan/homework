# CMake generated Testfile for 
# Source directory: /home/trc/Shenlan-autodriving/homework/project01_ws/src
# Build directory: /home/trc/Shenlan-autodriving/homework/project01_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("rosbridge_suite/rosbridge_suite")
subdirs("rosbridge_suite/rosapi")
subdirs("rosbridge_suite/rosbridge_library")
subdirs("rosbridge_suite/rosbridge_server")
subdirs("vehicle_control")
